import Const.{Coordinate, Direction, SIZED_HEIGHT, SIZED_WIDTH}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.Queue
import scala.collection.mutable.ArrayBuffer

object BetterDijkstra {
  @tailrec
  def generateDistanceMatrix(
                              nodeQueue: mutable.Queue[Coordinate],
                              distances: ArrayBuffer[ArrayBuffer[Int]],
                              visited: ArrayBuffer[ArrayBuffer[Boolean]],
                              occupiedPositions: ArrayBuffer[ArrayBuffer[Boolean]]
                            ): ArrayBuffer[ArrayBuffer[Int]]
  = {
    if (nodeQueue.isEmpty) {
      distances
    } else {
      val currentNode: Coordinate = nodeQueue.dequeue
      visited(currentNode._1)(currentNode._2) = true
      val neighbors = Position.getPredatorNeighbors(currentNode)

      neighbors.foreach(neighbor => {
        if (!visited(neighbor._1)(neighbor._2) && !nodeQueue.contains(neighbor) && !occupiedPositions(neighbor._1)(neighbor._2)) {
          nodeQueue += neighbor
          if (distances(neighbor._1)(neighbor._2) > distances(currentNode._1)(currentNode._2) + 1) {
            distances(neighbor._1)(neighbor._2) = distances(currentNode._1)(currentNode._2) + 1
          }
        }
      })
      generateDistanceMatrix(nodeQueue, distances, visited, occupiedPositions)
    }
  }


  
}
