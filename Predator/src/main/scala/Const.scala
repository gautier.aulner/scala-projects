import scala.collection.mutable.ArrayBuffer

object Const {

  type Coordinate = (Int, Int)
  val FRAME_RATE = 200
  
  val AGENT_SIZE = 10
  
  val WIDTH_BOUNDS: Int = 500
  val HEIGHT_BOUNDS: Int = 500

  val SIZED_WIDTH: Int = WIDTH_BOUNDS / AGENT_SIZE;
  val SIZED_HEIGHT: Int = HEIGHT_BOUNDS / AGENT_SIZE;

  enum Direction {
    case NORTH, EAST, SOUTH, WEST
  }
}
