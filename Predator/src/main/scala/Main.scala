import scalafx.application.{JFXApp3, Platform}
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.beans.property.{BooleanProperty, IntegerProperty, ObjectProperty}
import Const.Direction.*
import Const.{AGENT_SIZE, Coordinate, HEIGHT_BOUNDS, SIZED_HEIGHT, SIZED_WIDTH, WIDTH_BOUNDS}
import scalafx.scene.Scene
import javafx.scene.input.KeyCode
import scalafx.scene.paint.Color.*

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Main extends JFXApp3{
  private def gameLoop(update: () => Unit): Unit = {
    def tick = Future {
      update()
      Thread.sleep(Const.FRAME_RATE)
    }

    def loopAgain = Future(gameLoop(update))

    for {
      _ <- tick
      _ <- loopAgain
    } yield ()
  }

  def createOccupiedPositions(
                               obstacles: mutable.Queue[Obstacle],
                               occupiedPositions: ArrayBuffer[ArrayBuffer[Boolean]] = ArrayBuffer.fill(SIZED_WIDTH, SIZED_HEIGHT)(false)
                             )
  : ArrayBuffer[ArrayBuffer[Boolean]] =
  {
    if(obstacles.isEmpty){
      occupiedPositions
    } else {

      val obstacle = obstacles.dequeue
      for (i <- obstacle.position._1 to obstacle.position._1 + obstacle.width) {
        for(j <- obstacle.position._2 to obstacle.position._2 + obstacle.height) {
          occupiedPositions(i)(j) = true
        }
      }
      createOccupiedPositions(obstacles, occupiedPositions)
    }

  }

  override def start(): Unit = {
    val player = Player((400 / AGENT_SIZE, 400 / AGENT_SIZE))
    val predators: List[Predator] = List(
      Predator((50 / AGENT_SIZE, 50 / AGENT_SIZE)),
      Predator((100 / AGENT_SIZE, 400 / AGENT_SIZE))
    )
    val obstacles: mutable.Queue[Obstacle] = mutable.Queue(
      Obstacle((200 / AGENT_SIZE, 150 / AGENT_SIZE), 100  / AGENT_SIZE, 10 / AGENT_SIZE),
      Obstacle((200 / AGENT_SIZE, 150 / AGENT_SIZE), 10 / AGENT_SIZE, 200 / AGENT_SIZE),
      Obstacle((200 / AGENT_SIZE, 350 / AGENT_SIZE), 100 / AGENT_SIZE, 10 / AGENT_SIZE),
      Obstacle((200 / AGENT_SIZE, 250 / AGENT_SIZE), 80 / AGENT_SIZE, 10 / AGENT_SIZE),
      Obstacle((300 / AGENT_SIZE, 250 / AGENT_SIZE), 10 / AGENT_SIZE, 100 / AGENT_SIZE),
    )
    val obstaclesList = obstacles.toList
    val occupiedPositions: ArrayBuffer[ArrayBuffer[Boolean]] = createOccupiedPositions(obstacles)


    val state = ObjectProperty(State(player, predators, occupiedPositions, obstaclesList))
    val frame = IntegerProperty(0)
    val direction = IntegerProperty(4)

    frame.onChange {
      state.update(state.value.newState(direction.value))
    }

    stage = new PrimaryStage {
      title = "Predator"
      width = WIDTH_BOUNDS
      height = HEIGHT_BOUNDS
      scene = new Scene() {
        fill = Black
        frame.onChange(Platform.runLater {
          content = state.value.draw(direction.value)
        })
        onKeyPressed = { key =>
          key.getCode match {
            case KeyCode.UP    => direction.update(0);
            case KeyCode.RIGHT => direction.update(1);
            case KeyCode.DOWN  => direction.update(2);
            case KeyCode.LEFT  => direction.update(3);
            case _ => ();
          }
        }
      }
    }

    gameLoop(() => frame.update(frame.value + 1))
  }
}
