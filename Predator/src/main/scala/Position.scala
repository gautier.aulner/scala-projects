import Const.{AGENT_SIZE, Coordinate, Direction, SIZED_HEIGHT, SIZED_WIDTH}
import Const.Direction.*

import scala.collection.mutable.ArrayBuffer

object Position {

  def getPlayerPosition(direction: Int, position: Coordinate): Coordinate = {
    val (xMin, xMax, yMin, yMax) = (0, SIZED_WIDTH - 2, 0, SIZED_HEIGHT - 2)
    val x = position._1
    val y = position._2
    (direction, x, y) match {
      case (4, _, _) => (x, y)

      case (0, _, `yMin`) => (x, yMax)
      case (2, _, `yMax`) => (x, yMin)
      case (1, `xMax`, _) => (xMin, y)
      case (3, `xMin`, _) => (xMax, y)

      case (0, _, _) => (x, y - 1)
      case (2, _, _) => (x, y + 1)
      case (1, _, _) => (x + 1, y)
      case (3, _, _) => (x - 1, y)
    }
  }

  private def getPredatorPositionDijkstra(direction: Direction, position: Coordinate): Option[Coordinate] = {
    val (xMin, xMax, yMin, yMax) = (0, SIZED_WIDTH - 2, 0, SIZED_HEIGHT - 2)
    val x = position._1
    val y = position._2
    (direction, x, y) match {
      case(_, -1, -1) => None
      case(_, -1, _) => None
      case(_, _, -1) => None

      case (NORTH, _, `yMin`) => None
      case (SOUTH, _, `yMax`) => None
      case (EAST, `xMax`, _) => None
      case (WEST, `xMin`, _) => None

      case (NORTH, _, _) => Some((x, y - 1))
      case (SOUTH, _, _) => Some((x, y + 1))
      case (EAST, _, _) => Some((x + 1, y))
      case (WEST, _, _) => Some((x - 1, y))
    }
  }

  def getPredatorNeighbors(node: Coordinate): ArrayBuffer[Coordinate] = {
    val neighbors: ArrayBuffer[Coordinate] = ArrayBuffer()

    for (dir <- Direction.values) {
      val neighbor = Position.getPredatorPositionDijkstra(dir, node)
      if (neighbor.isDefined) {
        neighbors += neighbor.get
      }
    }

    neighbors
  }
}
