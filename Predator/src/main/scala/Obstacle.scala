import Const.Coordinate

case class Obstacle(position: Coordinate, width: Int, height: Int)
