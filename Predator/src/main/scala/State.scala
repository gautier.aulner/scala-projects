import Const.{AGENT_SIZE, Coordinate, Direction, HEIGHT_BOUNDS, SIZED_HEIGHT, SIZED_WIDTH, WIDTH_BOUNDS}
import scalafx.scene.shape.{Rectangle, Shape}
import scalafx.scene.text.Text
import scalafx.scene.paint.Color.*
import Position.{getPlayerPosition, getPredatorNeighbors}
import Direction.*

import scala.Console.println
import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

case class State(
                  player: Player,
                  predators: List[Predator],
                  occupiedPositions: ArrayBuffer[ArrayBuffer[Boolean]],
                  obstacles: List[Obstacle]
                ) {
  def newState(direction: Int): State = {
    //    if(player.position == predator.position) {
    if(predators.exists(predator => {
      predator.position == player.position
    })
    || direction == 4
    ) {
      // Ecran de mort ou de depart
      State(player, predators, occupiedPositions, obstacles)
    } else {


      val newPlayer = updatePlayer(player, direction)

      val baseDistances = ArrayBuffer.fill(SIZED_WIDTH, SIZED_HEIGHT)(Int.MaxValue)
      baseDistances(player.position._1)(player.position._2) = 0
      val distances = BetterDijkstra.generateDistanceMatrix(
        mutable.Queue[Coordinate](player.position),
        baseDistances,
        ArrayBuffer.fill(SIZED_WIDTH, SIZED_HEIGHT)(false),
        occupiedPositions
      )
//      println(distances)

//      val newPredator = predator.copy(getBestMove(distances, getPredatorNeighbors(predator.position), predator.position))
      val newPredators = predators.map(predator => {
        predator.copy(getBestMove(distances, getPredatorNeighbors(predator.position), predator.position))
      })

      State(newPlayer, newPredators, occupiedPositions, obstacles)
    }

  }

  @tailrec
  private def getBestMove(distances: ArrayBuffer[ArrayBuffer[Int]], neighbors: ArrayBuffer[Coordinate], move: Coordinate, index: Int = 0, minValue: Int = Int.MaxValue): Coordinate = {

    if(index == neighbors.length ){
      move
    } else {
      val neighbor = neighbors(index)
      if(distances(neighbor._1)(neighbor._2) < minValue){
        getBestMove(
          distances, neighbors, neighbor, index + 1, distances(neighbor._1)(neighbor._2)
        )
      } else {
        getBestMove(
          distances, neighbors, move, index + 1, minValue
        )
      }
    }
  }

  private def updatePlayer(player: Player, direction: Int): Player = {
    if(direction == 4){
      // Ne bouge pas
      player
    } else {
      player.copy(position = getPlayerPosition(direction, player.position))
    }
  }

  def draw(direction: Int): List[Shape] = {
    predators.exists(predator => {
      predator.position == player.position
    })
    if(predators.exists(predator => {
      predator.position == player.position
    })){
      List(
        new Text {
          text = "Dead"
          style = "-fx-font-size: 24px;"
          x = WIDTH_BOUNDS / 2
          y = HEIGHT_BOUNDS / 2
          fill = Red
        }
      )
    } else {
      val playerDraw: List[Rectangle] = List(
        new Rectangle {
          x = player.position._1 * AGENT_SIZE
          y = player.position._2 * AGENT_SIZE
          width = AGENT_SIZE
          height = AGENT_SIZE
          fill = Red
        }
      )

      val predatorsDraw: List[Rectangle] = predators.map(predator => {
        new Rectangle {
          x = predator.position._1 * AGENT_SIZE
          y = predator.position._2 * AGENT_SIZE
          width = AGENT_SIZE
          height = AGENT_SIZE
          fill = Green
        }
      })


      val obstaclesDraw: List[Rectangle] = obstacles.map(obstacle => {
        new Rectangle {
          x = obstacle.position._1 * AGENT_SIZE
          y = obstacle.position._2 * AGENT_SIZE
          width = obstacle.width * AGENT_SIZE
          height = obstacle.height * AGENT_SIZE
          fill = Gray
        }
      })
      if(direction == 4) {
        // Les différentes valueurs sont arbitraire et certaines sont pour les margins/padding
        val bgSquare = List(
          new Rectangle {
            x = 0
            y = HEIGHT_BOUNDS / 2 - AGENT_SIZE * 2
            width = WIDTH_BOUNDS
            height = 28
            fill = White
          }
        )
        val instructions = List(
          new Text {
            text = "Appuyer sur une flèche pour commencer"
            style = "-fx-font-size: 24px;"
            x = AGENT_SIZE
            y = HEIGHT_BOUNDS / 2
            fill = Black
          }
        )
        playerDraw ::: predatorsDraw ::: obstaclesDraw ::: bgSquare ::: instructions
      } else {
        playerDraw ::: predatorsDraw ::: obstaclesDraw
      }
    }
  }
}
