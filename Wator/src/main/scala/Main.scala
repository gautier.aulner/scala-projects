import scalafx.application.{JFXApp3, Platform}
import scalafx.beans.property.{IntegerProperty, ObjectProperty}
import Const.{CASE_SIZE, Coordinate, HEIGHT_BOUNDS, N_SHARK, N_TUNAS, SHARK_BREED_CYCLE, SHARK_ENERGY, TUNA_BREED_CYCLE, WIDTH_BOUNDS}
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.paint.Color.*

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global
import scala.util.Random

object Main extends JFXApp3{

  val ExistingPositions: ArrayBuffer[Coordinate] = ArrayBuffer()
  val FRAME_RATE = 50

  def generateRandomPostion(): Coordinate = {
    val x: Int = Random.nextInt(WIDTH_BOUNDS / CASE_SIZE)
    val y: Int = Random.nextInt(HEIGHT_BOUNDS / CASE_SIZE)
    if(ExistingPositions.contains((x, y))){
      generateRandomPostion()
    } else {
      ExistingPositions += new Coordinate(x, y)
      (x, y)
    }
  }

  def generateSharks(): ArrayBuffer[Shark] = {
    ArrayBuffer.fill(N_SHARK)(Shark(SHARK_BREED_CYCLE, SHARK_ENERGY, generateRandomPostion()))
  }

  def generateTunas(): ArrayBuffer[Tuna] = {
    ArrayBuffer.fill(N_TUNAS)(Tuna(TUNA_BREED_CYCLE, generateRandomPostion()))
  }

  def generateOccupiedPositionSharks(sharks: ArrayBuffer[Shark]): ArrayBuffer[ArrayBuffer[Boolean]] = {
    val occPos: ArrayBuffer[ArrayBuffer[Boolean]] = ArrayBuffer.fill(WIDTH_BOUNDS, HEIGHT_BOUNDS)(false)

    sharks.foreach(shark => {
      occPos(shark.position._1)(shark.position._2) = true
    })

    occPos
  }

  def generateOccupiedPositionTunas(tunas: ArrayBuffer[Tuna]): ArrayBuffer[ArrayBuffer[Boolean]] = {
    val occPos: ArrayBuffer[ArrayBuffer[Boolean]] = ArrayBuffer.fill(WIDTH_BOUNDS, HEIGHT_BOUNDS)(false)
    tunas.foreach(tuna => {
      occPos(tuna.position._1)(tuna.position._2) = true
    })
    occPos
  }

  def gameLoop(update: () => Unit): Unit = {
    def tick = Future {
      update()
      Thread.sleep(FRAME_RATE)
    }

    def loopAgain = Future(gameLoop(update))

    for {
      _ <- tick
      _ <- loopAgain
    } yield ()
  }

  override def start(): Unit = {

    val sharks: ArrayBuffer[Shark] = generateSharks()
    val tunas: ArrayBuffer[Tuna] = generateTunas()
    val state = ObjectProperty(State(sharks,tunas, generateOccupiedPositionSharks(sharks), generateOccupiedPositionTunas(tunas)))
    val frame = IntegerProperty(0) // boucle

    frame.onChange {
      state.update(state.value.newState())
    }
    
    stage = new PrimaryStage {
      title = "WATOR"
      width = WIDTH_BOUNDS
      height = HEIGHT_BOUNDS
      scene = new Scene() {
        fill = Black
        frame.onChange(Platform.runLater {
          content = state.value.draw()
        })
      }
    }

    gameLoop(() => frame.update(frame.value + 1))
  }
}