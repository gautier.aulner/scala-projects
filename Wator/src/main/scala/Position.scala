import Const.Coordinate

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

object Position {

  def neighboursOf(x: Int, y: Int, distance: Int): Seq[Coordinate] = {
    val allPositions = for {
      i <- -distance to distance
      j <- -distance to distance if (i, j) != (0, 0)
    } yield (x + i, y + j )
    allPositions.filter {
      position => {
        position._1 > 0 && position._2 > 0 && position._1 < Const.WIDTH_BOUNDS / Const.CASE_SIZE && position._2 < Const.HEIGHT_BOUNDS / Const.CASE_SIZE
      }
    }
  }


  def sharkMove(sharkPosition: Coordinate, tunaPlacement: ArrayBuffer[ArrayBuffer[Boolean]], sharkPlacement: ArrayBuffer[ArrayBuffer[Boolean]]): Coordinate = {
    val positionList = neighboursOf(sharkPosition._1, sharkPosition._2, 1)

    val tunaAvailable = positionList.filter {
      position => {
        tunaPlacement(position._1)(position._2)
      }
    }
    if(tunaAvailable.length > 0){
      val random = new Random().nextInt(tunaAvailable.length)
      tunaAvailable(random)
    } else {
      baseMove(sharkPosition, tunaPlacement, sharkPlacement)
    }
  }

  def baseMove(basePosition: Coordinate, tunaPlacement: ArrayBuffer[ArrayBuffer[Boolean]], sharkPlacement: ArrayBuffer[ArrayBuffer[Boolean]]): Coordinate = {
    val positionList = neighboursOf(basePosition._1, basePosition._2, 1)

    val availablePosition = positionList.filter {
      position => {
        !(tunaPlacement(position._1)(position._2) || sharkPlacement(position._1)(position._2))
      }
    }
    if (availablePosition.length > 0) {
      val random = new Random().nextInt(availablePosition.length)
      availablePosition(random)
    } else {
      basePosition
    }
  }

}
