
import scala.collection.mutable.ArrayBuffer
import scalafx.scene.shape.{Ellipse, Polygon, Rectangle, Shape}
import scalafx.scene.paint.Color.*
import Const.{CASE_SIZE, Coordinate, HEIGHT_BOUNDS, SHARK_BREED_CYCLE, SHARK_ENERGY, TUNA_BREED_CYCLE, TUNA_ENERGY_VALUE, WIDTH_BOUNDS}
import javafx.scene.paint.ImagePattern
import scalafx.scene.image.Image
import scalafx.scene.paint.{Color, Paint}

import scala.util.Random


case class State(sharks: ArrayBuffer[Shark], tunas: ArrayBuffer[Tuna], sharkPlacement: ArrayBuffer[ArrayBuffer[Boolean]], tunaPlacement: ArrayBuffer[ArrayBuffer[Boolean]]) {
  def newState(): State = {

    val aliveShark = sharks.filter {
      shark => {
        shark.energy > 0
      }
    }

    val newSharks = aliveShark.flatMap {
      shark => {
        updateShark(shark)
      }
    }

    val aliveTunas = tunas.filter {
      tuna => {
        !sharkPlacement(tuna.position._1)(tuna.position._2)
      }
    }

    val newTunas = aliveTunas.flatMap {
      tuna => {
        updateTuna(tuna)
      }
    }

    State(
      newSharks, newTunas, sharkPlacement, tunaPlacement
    )
  }



  def updateShark(shark: Shark): ArrayBuffer[Shark] = {
    val newSharks: ArrayBuffer[Shark] = new ArrayBuffer[Shark]()
    sharkPlacement(shark.position._1)(shark.position._2) = false

    val newPos: Coordinate = Position.sharkMove(shark.position, tunaPlacement, sharkPlacement)
    
    val canBreed: Boolean = shark.timeBeforeBreed <= 0
    val isMoving: Boolean = newPos != shark.position
    if (canBreed && isMoving) {
      sharkPlacement(shark.position._1)(shark.position._2) = true
      newSharks += Shark(SHARK_BREED_CYCLE, SHARK_ENERGY, shark.position)
    }

    val newTimeBeforeBreed: Int =
      if(!isMoving) shark.timeBeforeBreed
      else if(canBreed && !isMoving) 0
      else if(canBreed && isMoving) SHARK_BREED_CYCLE
      else shark.timeBeforeBreed - 1
    
    sharkPlacement(newPos._1)(newPos._2) = true
    
    if (tunaPlacement(shark.position._1)(shark.position._2)) {
      tunaPlacement(shark.position._1)(shark.position._2) = false
      newSharks += Shark(newTimeBeforeBreed, shark.energy + TUNA_ENERGY_VALUE, newPos)
    } else {
      newSharks += Shark(newTimeBeforeBreed, shark.energy - 1, newPos)
    }
    
    newSharks
  }

  def updateTuna(tuna: Tuna): ArrayBuffer[Tuna] = {
    val newTunas: ArrayBuffer[Tuna] = new ArrayBuffer[Tuna]()
    tunaPlacement(tuna.position._1)(tuna.position._2) = false

    val newPos: Coordinate = Position.baseMove(tuna.position, tunaPlacement, sharkPlacement)

    val canBreed: Boolean = tuna.timeBeforeBreed <= 0 && newPos != tuna.position
    if(canBreed){
      tunaPlacement(tuna.position._1)(tuna.position._2) = true
      newTunas += Tuna(TUNA_BREED_CYCLE, tuna.position, Color.Green)
    }

    tunaPlacement(newPos._1)(newPos._2) = true
    if(canBreed) {
      newTunas += Tuna(TUNA_BREED_CYCLE, newPos, tuna.color)
    } else {
      newTunas += Tuna(tuna.timeBeforeBreed - 1, newPos, tuna.color)
    }
    newTunas
  }

  def draw(): List[Shape] = {

    val sharkRectangles: List[Rectangle] = sharks.map {
      shark => {
        new Rectangle{
          x = shark.position._1 * CASE_SIZE
          y = shark.position._2 * CASE_SIZE
          width = CASE_SIZE
          height = CASE_SIZE
          fill = Color.Blue
        }
      }
    }.toList
    val traingleVariation: Double = 0.6
    val tunasRectangles: List[Shape] = tunas.flatMap {
      tuna => {
        List(
          new Polygon {
            fill = tuna.color
            points.addAll(
              (tuna.position._1.toDouble - traingleVariation) * CASE_SIZE, (tuna.position._2.toDouble - traingleVariation) * CASE_SIZE,
              (tuna.position._1.toDouble - traingleVariation) * CASE_SIZE, (tuna.position._2.toDouble + traingleVariation) * CASE_SIZE,
              (tuna.position._1.toDouble + traingleVariation) * CASE_SIZE, (tuna.position._2.toDouble) * CASE_SIZE,
            )
          },
          new Ellipse {
            centerX = (tuna.position._1 + traingleVariation) * CASE_SIZE
            centerY = tuna.position._2 * CASE_SIZE
            radiusX = 2
            radiusY = 2
            fill = tuna.color
          }
        )
      }
    }.toList
    sharkRectangles ::: tunasRectangles
  }

}
