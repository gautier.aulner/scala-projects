import Const.Coordinate
import scalafx.scene.paint.Color

case class Tuna(timeBeforeBreed: Int, position: Coordinate, color: Color = Color.Green)