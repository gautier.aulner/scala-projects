import scalafx.scene.paint.Color
import scalafx.scene.paint.Color.*
import scalafx.stage.Screen

object Const {
  
  val CASE_SIZE = 5

  val WIDTH_BOUNDS: Int = 600
  val HEIGHT_BOUNDS: Int = 600


  val N_TUNAS: Int = 200
  val TUNA_BREED_CYCLE: Int = 50
  val TUNA_ENERGY_VALUE: Int = 20

  val N_SHARK: Int = 100
  val SHARK_BREED_CYCLE: Int = 210
  val SHARK_ENERGY: Int = 200

  type Coordinate = (Int, Int)

}
