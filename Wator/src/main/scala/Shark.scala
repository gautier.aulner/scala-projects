import Const.Coordinate

import scala.collection.mutable.ArrayBuffer

case class Shark(timeBeforeBreed: Int, energy: Int, position: Coordinate)
