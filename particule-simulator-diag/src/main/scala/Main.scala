import javafx.scene.input.KeyCode
import scalafx.application.{JFXApp3, Platform}
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.beans.property.{IntegerProperty, ObjectProperty}
import scalafx.scene.Scene
import scalafx.scene.paint.Color.*
import scalafx.scene.shape.{Circle, Rectangle}
import scalafx.stage.Screen

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global
import scala.util.Random
import Const.DirectionDiagonal.*
import Const.{HEIGHT_BOUNDS, SCREEN_HEIGHT, SCREEN_WIDTH, WIDTH_BOUNDS}
import javafx.beans.property.ListProperty
import scalafx.scene.paint.Color

import scala.collection.mutable.ArrayBuffer

object Main extends JFXApp3 {

  val NUMBER_OF_PARTICULE: Int = 5000
  val PARTICULE_SIZE: Int = 2

  def generateRandomCoordWidth: Int = {
    Random.nextInt(SCREEN_WIDTH / PARTICULE_SIZE)
  }

  def generateRandomCoordHeight: Int = {
    Random.nextInt(SCREEN_HEIGHT / PARTICULE_SIZE)
  }

  def generateRandomColor: Color = {
    Color.rgb(Random.nextInt(256),Random.nextInt(256),Random.nextInt(256),1)
  }

  def generateRandomDirectionDiagonal: Const.DirectionDiagonal = {
    val random = Random.nextInt(8)
    random match
      case 0 => NORTH
      case 1 => NORTH_EAST
      case 2 => EAST
      case 3 => SOUTH_EAST
      case 4 => SOUTH
      case 5 => SOUTH_WEST
      case 6 => WEST
      case 7 => NORTH_WEST
  }

  def gameLoop(update: () => Unit): Unit = {
    def tick = Future {
      update()
      Thread.sleep(34)
    }
    def loopAgain = Future(gameLoop(update))

    for {
      _ <- tick
      _ <- loopAgain
    } yield ()
  }

  def generateParticuleList: List[Particule] = {
    List.fill(NUMBER_OF_PARTICULE)(
      Particule(
        (generateRandomCoordWidth,generateRandomCoordHeight),
        generateRandomColor,
        generateRandomDirectionDiagonal
      )
    )
  }

  def generateOccupiedPosition(particules: List[Particule]): ArrayBuffer[ArrayBuffer[Boolean]] = {
    val occPos = ArrayBuffer.fill(WIDTH_BOUNDS, HEIGHT_BOUNDS)(false)
    particules.foreach(particule => {
      occPos(particule.position._1)(particule.position._2) = true
    })
    occPos
  }

  override def start(): Unit = {
    val particules = generateParticuleList

    val state = ObjectProperty(State(particules, generateOccupiedPosition(particules)))
    val frame = IntegerProperty(0) // boucle

    val screenWidth = IntegerProperty(SCREEN_WIDTH)
    val screenHeight = IntegerProperty(SCREEN_HEIGHT)

    frame.onChange {
      state.update(state.value.newState())
    }

    stage = new PrimaryStage{
      title = "Particule Simulator"
      width = SCREEN_WIDTH
      height = SCREEN_HEIGHT
      scene = new Scene() {
        fill = Black
        frame.onChange(Platform.runLater {
          content = state.value.draw()
        })

        width.onChange { (_, _, newWidth) =>
          screenWidth.update(newWidth.intValue())
        }

        height.onChange { (_, _, newHeight) =>
          screenHeight.update(newHeight.intValue())

        }
      }
    }
    gameLoop(() => frame.update(frame.value + 1))
  }
}
