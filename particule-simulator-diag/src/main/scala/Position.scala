import Const.DirectionDiagonal.*

object Position {
  def getPositions(direction: Const.DirectionDiagonal, x: Int, y: Int) = {
    val (xMin, xMax, yMin, yMax) = (0, Const.WIDTH_BOUNDS - 1, 0, Const.HEIGHT_BOUNDS - 1)
    (direction, x, y) match {
      case (NORTH, _, `yMin`) => (x, yMax) // tout en haut => tout en bas
      case (SOUTH, _, `yMax`) => (x, yMin) // tout en bas => tout en haut
      case (EAST, `xMax`, _) => (xMin, y) // tout à droite => tout à gauche
      case (WEST, `xMin`, _) => (xMax, y) // tout à gauche => tout à droite
      case (NORTH_EAST, `xMax`, `yMin`) => (xMin, yMax) // tout en haut à droite => tout en bas à gauche
      case (NORTH_WEST, `xMin`, `yMin`) => (xMax, yMax) // tout en haut à gauche => tout en bas à droite
      case (SOUTH_EAST, `xMax`, `yMax`) => (xMin, yMin) // tout en bas à droite => tout en haut à gauche
      case (SOUTH_WEST, `xMin`, `yMax`) => (xMax, yMin) // tout en bas à gauche => tout en haut à droite
      
      case (NORTH_EAST, `xMax`, _) => (xMin, y)
      case (NORTH_EAST, _, `yMin`) => (x, yMax)
      
      case (NORTH_WEST, `xMin`, _) => (xMax, y)
      case (NORTH_WEST, _, `yMin`) => (x, yMax)
      
      case (SOUTH_EAST, `xMax`, _) => (xMin, y)
      case (SOUTH_EAST, _, `yMax`) => (x, yMin) 
      
      case (SOUTH_WEST, `xMin`, _) => (xMax, y)
      case (SOUTH_WEST, _, `yMax`) => (x, yMin)
      
      case (NORTH, _, _) => (x, y - 1)
      case (SOUTH, _, _) => (x, y + 1)
      case (EAST, _, _) => (x + 1, y)
      case (WEST, _, _) => (x - 1, y)
      case (NORTH_EAST, _, _) => (x + 1, y - 1)
      case (NORTH_WEST, _, _) => (x - 1, y - 1)
      case (SOUTH_EAST, _, _) => (x + 1, y + 1)
      case (SOUTH_WEST, _, _) => (x - 1, y + 1)
    }
  }
}