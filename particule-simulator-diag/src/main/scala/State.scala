import scalafx.scene.shape.Circle
import scalafx.stage.Screen
import Const.{DirectionDiagonal, HEIGHT_BOUNDS, WIDTH_BOUNDS}
import Main.PARTICULE_SIZE
import Const.DirectionDiagonal.*
import scalafx.scene.paint.Color
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

case class State(allParticles: List[Particule], canvas: ArrayBuffer[ArrayBuffer[Boolean]]) {
  def newState(): State = {

    val newAllParticles: List[Particule] = allParticles.map {
      particle => {
        moveParticule(particle)
      }
    }

    State(
      newAllParticles, canvas
    )
  }

  def moveParticule(particle: Particule): Particule = {
    canvas(particle.position._1)(particle.position._2) = false
    val newPos: (Int, Int) = Position.getPositions(particle.direction, particle.position._1, particle.position._2)

    if(canvas(newPos._1)(newPos._2)){
      canvas(particle.position._1)(particle.position._2) = true
      Particule((particle.position._1, particle.position._2), particle.color, getCollidingDirection((particle.direction)))
    } else {
      canvas(newPos._1)(newPos._2) = true
      Particule((newPos._1, newPos._2), particle.color, particle.direction)
    }
  }

  def getCollidingDirection(direction: DirectionDiagonal): DirectionDiagonal = {
    val random = Random.nextInt(8)
    val newDirection: DirectionDiagonal = random match
      case 0 => NORTH
      case 1 => NORTH_EAST
      case 2 => EAST
      case 3 => SOUTH_EAST
      case 4 => SOUTH
      case 5 => SOUTH_WEST
      case 6 => WEST
      case 7 => NORTH_WEST
    if(newDirection == direction){
      getCollidingDirection(direction)
    } else {
      newDirection
    }
  }

  def draw(): List[Circle] = {
    allParticles.map {
      particule => {
        new Circle {
          centerX = particule.position._1 * Main.PARTICULE_SIZE
          centerY = particule.position._2 * Main.PARTICULE_SIZE
          radius = Main.PARTICULE_SIZE / 2
          fill = particule.color
        }
      }
    }
  }
}
