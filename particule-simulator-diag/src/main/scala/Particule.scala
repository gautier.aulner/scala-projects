import scalafx.scene.paint.Color
import scalafx.scene.paint.Color.*
case class Particule(position: (Int, Int), color: Color, direction: Const.DirectionDiagonal)