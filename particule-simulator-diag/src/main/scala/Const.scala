import scalafx.scene.paint.Color
import scalafx.scene.paint.Color.*
import scalafx.stage.Screen
object Const {

  enum DirectionDiagonal {
    case NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST
  }



  val WIDTH_BOUNDS: Int = Screen.primary.visualBounds.width.toInt / Main.PARTICULE_SIZE
  val HEIGHT_BOUNDS: Int = Screen.primary.visualBounds.height.toInt / Main.PARTICULE_SIZE

  val SCREEN_WIDTH: Int = Screen.primary.visualBounds.width.toInt
  val SCREEN_HEIGHT: Int = Screen.primary.visualBounds.height.toInt

}
